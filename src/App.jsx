import React, {Component} from "react";
import {BrowserRouter as Router, Route, Redirect, Switch, NavLink} from "react-router-dom";
import {find} from 'lodash';

import AsyncComponent from "./components/AsyncComponent";
import NotFound from "./components/404";
import styles from './style.css';

const tabs = require('../config/tabs');

class App extends Component {
    constructor() {
        super();
        this.state = {
            tabs: tabs.sort((a, b) => a.order - b.order),
            defaultTab: find(tabs, {order: 0}).id
        }
    }

    render() {
        let {tabs} = this.state;
        let {defaultTab} = this.state;

        return (
            <Router>
                <div>
                    <div className={styles.navigation}>
                        {tabs.map(tab => <NavLink
                            activeClassName="active"
                            className={`${styles.link} ${styles.active}`}
                            key={tab.id}
                            to={`/${tab.id}`}>{tab.title}</NavLink>)}
                    </div>
                    <hr/>
                    <Switch>
                        {tabs.map(tab => <Route
                            key={tab.id}
                            path={`/${tab.id}`}
                            component={() => <AsyncComponent
                                moduleProvider={() => import(`./components/${tab.path}`)}/>}/>)}
                        <Route exact path="/" render={() => <Redirect to={{pathname: `/${defaultTab}`}}/>}/>
                        <Route component={NotFound}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;